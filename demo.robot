*** Settings ***
Resource    resource.robot
Suite Setup    Open Browser And Login
# Suite Teardown    Close Browser

*** Test Cases ***
Test Case 1
    Click Element    id=nav_id_MediaVault
    Sleep    2s
    Click Element    id=btn_createWorkspace
    Sleep    2s
    Input Text    id=txt_workspaceName    TestCreate
    Sleep    2s
    Execute JavaScript    saveWorkspace()
    Sleep    5s
