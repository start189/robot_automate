*** Settings ***
Library    SeleniumLibrary
 
*** Variables *** 
${URL}  http://202.139.213.88:8080/rtpfg/authentication/home.action
${NAME_TEXT}  chakrit.b@ddlg-corp.com
${PASSWORD_TEXT}  luffy
${NAME_WORKSPACE}    TestBot22
*** Test Cases ***
Test Case 1 
    Open Browser    ${URL}    chrome
    Wait Until Page Contains Element    name:login
    Sleep    2s    # หยุดการทำงานเป็นเวลา 5 วินาที
    Input Text    id: username   ${NAME_TEXT}
    Input Text    id: userpass   ${PASSWORD_TEXT}
    Sleep    2s    # หยุดการทำงานเป็นเวลา 5 วินาที
    Click Button    css: .btn.btn-BloodRed 
    Sleep    5s    # หยุดการทำงานเป็นเวลา 5 วินาที
    Close Browser
Test Case 2 
    Open Browser    ${URL}    chrome
    Wait Until Page Contains Element    name:login
    Sleep    2s    # หยุดการทำงานเป็นเวลา 5 วินาที
    Input Text    id: username   ${NAME_TEXT}
    Input Text    id: userpass   ${PASSWORD_TEXT}
    Sleep    2s    # หยุดการทำงานเป็นเวลา 5 วินาที
    Click Button    css: .btn.btn-BloodRed 
    # Wait Until Page Contains Element    id:side-nav
    Sleep    2s 
    Click Element    id: nav_id_MediaVault
    Sleep    2s
    Click Element  id: btn_createWorkspace
    Sleep    2s
    Input Text    Id: txt_workspaceName  ${NAME_WORKSPACE}
    Sleep    2s
    Execute JavaScript    saveWorkspace()
    Sleep    10s
    Close Browser