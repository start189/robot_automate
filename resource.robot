*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${URL}    http://202.139.213.88:8080/rtpfg/authentication/login.action
${BROWSER}    chrome
${USERNAME}    chakrit.b@ddlg-corp.com
${PASSWORD}    luffy

*** Keywords ***
Open Browser And Login
    Open Browser    ${URL}   ${BROWSER}    options=add_argument("--ignore-certificate-errors")
    Maximize Browser Window
    Input Text    name=loginForm.userName    ${USERNAME}
    Input Text    name=loginForm.userPass    ${PASSWORD}
    Click Button    css:.btn.btn-BloodRed 
    Sleep    5s
    ${current_url}=    Get Location
    Log    Current URL after login: ${current_url}
    Run Keyword If    '${current_url}' == '${URL}'    Fail    Login failed, still on login page
    Location Should Contain    home.action
    Log    Login successful

Close Browser
    # [Teardown]    Close All Browser
